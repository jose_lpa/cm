#include "datetime-decorator-tests.h"

#include <QSignalSpy>

#include <data/entity.h>

namespace cm {
namespace data {  // Instance.

static DateTimeDecoratorTests instance;

DateTimeDecoratorTests::DateTimeDecoratorTests() :
    TestSuite("DateTimeDecoratorTests")
{
}

}

namespace data {  // Tests.

void DateTimeDecoratorTests::constructor_givenParameters_setsProperties()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);

    QCOMPARE(decorator.parentEntity(), &parentEntity);
    QCOMPARE(decorator.key(), QString("Test Key"));
    QCOMPARE(decorator.label(), QString("Test Label"));
    QCOMPARE(decorator.value(), testDateTime);
}

void DateTimeDecoratorTests::constructor_givenNoParameters_setsDefaultProperties()
{
    DateTimeDecorator decorator;

    QCOMPARE(decorator.parentEntity(), nullptr);
    QCOMPARE(decorator.key(), QString("SomeItemKey"));
    QCOMPARE(decorator.label(), QString(""));
    QCOMPARE(decorator.value(), QDateTime());
}

void DateTimeDecoratorTests::setValue_givenNewValue_updatesValueAndEmitsSignal()
{
    DateTimeDecorator decorator;
    QSignalSpy valueChangedSpy(&decorator, &DateTimeDecorator::valueChanged);

    QCOMPARE(decorator.value(), QDateTime());
    decorator.setValue(testDateTime);
    QCOMPARE(decorator.value(), testDateTime);
    QCOMPARE(valueChangedSpy.count(), 1);
}

void DateTimeDecoratorTests::setValue_givenSameValue_takesNoAction()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);
    QSignalSpy valueChangedSpy(&decorator, &DateTimeDecorator::valueChanged);

    QCOMPARE(decorator.value(), testDateTime);
    decorator.setValue(testDateTime);
    QCOMPARE(decorator.value(), testDateTime);
    QCOMPARE(valueChangedSpy.count(), 0);
}

void DateTimeDecoratorTests::jsonValue_whenDefaultValue_returnsJson()
{
    DateTimeDecorator decorator;

    QCOMPARE(decorator.jsonValue(),
             QJsonValue(QDateTime().toString(Qt::ISODate)));
}

void DateTimeDecoratorTests::jsonValue_whenValueSet_returnsJson()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);

    QCOMPARE(decorator.jsonValue(),
             QJsonValue(testDateTime.toString(Qt::ISODate)));
}

void DateTimeDecoratorTests::update_whenPresentInJson_updatesValue()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);
    QSignalSpy valueChangedSpy(&decorator, &DateTimeDecorator::valueChanged);
    QDateTime timestamp = QDateTime(QDate(2020, 2, 10), QTime(12, 35, 02));

    QCOMPARE(decorator.value(), testDateTime);
    QJsonObject jsonObject;
    jsonObject.insert("Key 1", "Value 1");
    jsonObject.insert("Test Key", timestamp.toString(Qt::ISODate));
    jsonObject.insert("Key 3", 3);
    decorator.update(jsonObject);
    QCOMPARE(decorator.value(), timestamp);
    QCOMPARE(valueChangedSpy.count(), 1);
}

void DateTimeDecoratorTests::update_whenNotPresentInJson_updatesValueToDefault()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);
    QSignalSpy valueChangedSpy(&decorator, &DateTimeDecorator::valueChanged);
    QDateTime timestamp = QDateTime(QDate(2020, 2, 10), QTime(12, 35, 02));

    QCOMPARE(decorator.value(), testDateTime);
    QJsonObject jsonObject;
    jsonObject.insert("Key 1", "Value 1");
    jsonObject.insert("Key 2", timestamp.toString(Qt::ISODate));
    jsonObject.insert("Key 3", 3);
    decorator.update(jsonObject);
    QCOMPARE(decorator.value(), QDateTime());
    QCOMPARE(valueChangedSpy.count(), 1);
}

void DateTimeDecoratorTests::toIso8601String_whenDefaultValue_returnsString()
{
    DateTimeDecorator decorator;

    QCOMPARE(decorator.toIso8601String(), QString(""));
}

void DateTimeDecoratorTests::toIso8601String_whenValueSet_returnsString()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);

    QCOMPARE(decorator.toIso8601String(), testDateTime.toString(Qt::ISODate));
}

void DateTimeDecoratorTests::toPrettyDateString_whenDefaultValue_returnsString()
{
    DateTimeDecorator decorator;

    QCOMPARE(decorator.toPrettyDateString(), QString("Not set"));
}

void DateTimeDecoratorTests::toPrettyDateString_whenValueSet_returnsString()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);

    QCOMPARE(decorator.toPrettyDateString(),
             testDateTime.toString("d MMM yyy"));
}

void DateTimeDecoratorTests::toPrettyTimeString_whenDefaultValue_returnsString()
{
    DateTimeDecorator decorator;

    QCOMPARE(decorator.toPrettyTimeString(), QString("Not set"));
}

void DateTimeDecoratorTests::toPrettyTimeString_whenValueSet_returnsString()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);

    QCOMPARE(decorator.toPrettyTimeString(), testDateTime.toString("hh:mm ap"));
}

void DateTimeDecoratorTests::toPrettyString_whenDefaultValue_returnsString()
{
    DateTimeDecorator decorator;

    QCOMPARE(decorator.toPrettyString(), QString(""));
}

void DateTimeDecoratorTests::toPrettyString_whenValueSet_returnsString()
{
    Entity parentEntity;
    DateTimeDecorator decorator(
                &parentEntity, "Test Key", "Test Label", testDateTime);

    QCOMPARE(decorator.toPrettyString(),
             testDateTime.toString("ddd d MMM yyy @ HH:mm:ss"));

}

}
}
