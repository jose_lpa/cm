#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <controllers/master-controller.h>
#include <controllers/command-controller.h>
#include <data/datetime-decorator.h>
#include <data/enumerator-decorator.h>
#include <data/int-decorator.h>
#include <data/string-decorator.h>
#include <models/address.h>
#include <models/appointment.h>
#include <models/client.h>
#include <models/client-search.h>
#include <models/contact.h>
#include <rss/rss-channel.h>
#include <rss/rss-image.h>
#include <rss/rss-item.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    /* Registering the type with the QML engine. Note that the template
     * parameter must be fully qualified with all namespaces. We will add
     * the type’s metadata into a module called CM with a version number
     * 1.0, and we want to refer to this type as `MasterController` in QML
     * markup.
     */
    qmlRegisterType<cm::controllers::NavigationController>(
                "CM", 1, 0, "NavigationController");
    qmlRegisterType<cm::controllers::CommandController>(
                "CM", 1, 0, "CommandController");
    qmlRegisterType<cm::framework::Command>("CM", 1, 0, "Command");

    qmlRegisterType<cm::data::DateTimeDecorator>(
                "CM", 1, 0, "DateTimeDecorator");
    qmlRegisterType<cm::data::EnumeratorDecorator>(
                "CM", 1, 0, "EnumeratorDecorator");
    qmlRegisterType<cm::data::IntDecorator>("CM", 1, 0, "IntDecorator");
    qmlRegisterType<cm::data::StringDecorator>("CM", 1, 0, "StringDecorator");

    qmlRegisterType<cm::models::Address>("CM", 1, 0, "Address");
    qmlRegisterType<cm::models::Appointment>("CM", 1, 0, "Appointment");
    qmlRegisterType<cm::models::Client>("CM", 1, 0, "Client");
    qmlRegisterType<cm::models::ClientSearch>("CM", 1, 0, "ClientSearch");
    qmlRegisterType<cm::models::Contact>("CM", 1, 0, "Contact");
    qmlRegisterType<cm::rss::RssChannel>("CM", 1, 0, "RssChannel");
    qmlRegisterType<cm::rss::RssImage>("CM", 1, 0, "RssImage");
    qmlRegisterType<cm::rss::RssItem>("CM", 1, 0, "RssItem");

    // Instantiate an instance of `MasterController`.
    cm::controllers::MasterController masterController;

    QQmlApplicationEngine engine;

    // Add the UI resources import path.
    engine.addImportPath("qrc:/");

    // Inject the `masterController` into root QML context.
    engine.rootContext()->setContextProperty("masterController",
                                             &masterController);

    const QUrl url(QStringLiteral("qrc:/views/MasterView.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
