#-------------------------------------------------
#
# Project created by QtCreator 2019-09-07T22:59:37
#
#-------------------------------------------------

include(../qmake-target-platform.pri)
include(../qmake-destination-path.pri)

QT -= gui
QT += sql network xml

TARGET = cm-lib
TEMPLATE = lib

CONFIG += c++14

DEFINES += CMLIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += source \
    ../cm-lib/source

SOURCES += \
        source/controllers/command-controller.cpp \
        source/controllers/database-controller.cpp \
        source/controllers/master-controller.cpp \
        source/data/data-decorator.cpp \
        source/data/datetime-decorator.cpp \
        source/data/entity.cpp \
        source/data/enumerator-decorator.cpp \
        source/data/int-decorator.cpp \
        source/data/string-decorator.cpp \
        source/framework/command.cpp \
        source/models/address.cpp \
        source/models/appointment.cpp \
        source/models/client-search.cpp \
        source/models/client.cpp \
        source/models/contact.cpp \
        source/networking/network-access-manager.cpp \
        source/networking/web-request.cpp \
        source/rss/rss-channel.cpp \
        source/rss/rss-image.cpp \
        source/rss/rss-item.cpp \
        source/utilities/xml-helper.cpp

HEADERS += \
        source/controllers/command-controller.h \
        source/controllers/database-controller.h \
        source/controllers/i-database-controller.h \
        source/controllers/master-controller.h \
        source/controllers/navigation-controller.h \
        source/data/data-decorator.h \
        source/data/datetime-decorator.h \
        source/data/entity-collection.h \
        source/data/entity.h \
        source/data/enumerator-decorator.h \
        source/data/int-decorator.h \
        source/data/string-decorator.h \
        source/framework/command.h \
        source/models/address.h \
        source/models/appointment.h \
        source/models/client-search.h \
        source/models/client.h \
        source/cm-lib_global.h \
        source/models/contact.h \
        source/networking/i-network-access-manager.h \
        source/networking/i-web-request.h \
        source/networking/network-access-manager.h \
        source/networking/web-request.h \
        source/rss/rss-channel.h \
        source/rss/rss-image.h \
        source/rss/rss-item.h \
        source/utilities/xml-helper.h

DESTDIR = $$PWD/../binaries/$$DESTINATION_PATH
OBJECTS_DIR = $$PWD/build/$$DESTINATION_PATH/.obj
MOC_DIR = $$PWD/build/$$DESTINATION_PATH/.moc
RCC_DIR = $$PWD/build/$$DESTINATION_PATH/.qrc
UI_DIR = $$PWD/build/$$DESTINATION_PATH/.ui

message(cm-lib output dir: $${DESTDIR})

unix {
    target.path = /usr/lib
    INSTALLS += target
}
